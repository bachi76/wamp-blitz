package ch.insign.blitz;

import com.fasterxml.jackson.databind.JsonNode;
import ws.wamp.jawampa.Request;

import java.lang.reflect.Method;

/**
 * The Blitz Controller interface allowes Blitz to route client events to methods.
 * The default dispatcher just tries to call public methods with the same name as the event
 * and the request as argument.
 *
 * Action method signature:
 *
 * 		public void someEventName(Request request, Blitz blitz) {
 * 		    JsonNode data = request.arguments().get(1);
 * 		}
 */
public interface BlitzController {

	/**
	 * A simple dispatcher that calls the method named like the event
	 */
	default void dispatchEvent(String event, Request request, Blitz blitz) {
		try {
			Method method = this.getClass().getMethod(event, Request.class, Blitz.class);
			method.setAccessible(true); // required even though the target method is public
			method.invoke(this, request, blitz);

		} catch (Exception e) {
			// Consume exceptions here so the wamp channel isnt affected
			e.printStackTrace();
		}
	}

	/**
	 * A new client has connected - send initial input.
	 */
	void connected(Request request, Blitz blitz);


	/**
	 * A client has disconnected.
	 * (Note: the disconnected call is browser-initiated (onUnload) and not guaranteed)
	 */
	default void disconnected(Request request, Blitz blitz) {}


	// FIXME: Add verbose error handling

	/**
	 * Get the WAMP Session Id from the request.
	 * @param request the caller request
	 * @return sid as String
	 */
	default String getSid(Request request) {
		return request.details().get("caller").asText();
	}

	default JsonNode getData(Request request) {
		JsonNode data = request.arguments().get(1);
		return data;
	}
}
