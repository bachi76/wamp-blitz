package ch.insign.blitz;


import org.slf4j.LoggerFactory;
import ws.wamp.jawampa.*;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;
import ws.wamp.jawampa.transport.netty.SimpleWampWebsocketListener;

import java.net.URI;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Blitz
 *
 * @author Martin Bachmann
 */

public class Blitz {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(Blitz.class);

	private static final String URI_PREFIX = "ch.insign.blitz.";
	private static final String URI_SERVER_CALL = Blitz.URI_PREFIX + "server.call" ;
	private static final String URI_SERVER_DATA = Blitz.URI_PREFIX + "server.data" ;
	private static final String URI_CLIENT_CALL = Blitz.URI_PREFIX + "client.data.to." ; // + sid
	private static final String EVENT_CONNECTED = "connected";
	private static final String EVENT_DISCONNECTED = "disconnected";

	private final String wsUrl;
	private final String realm;

	private WampRouter router;
	private SimpleWampWebsocketListener server;
	private IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();
	private WampClient wampClient;

	public Blitz(String wsUrl, String realm) {
		this.wsUrl = wsUrl;
		this.realm = realm;
	}

	/**
	 * Returns the wamp client or null if not available yet
	 */
	public WampClient client() {
		return wampClient;
	}

	/**
	 * Start the routing service and its websocket listener
	 */
	public void startRouter() throws ApplicationError {
		WampRouterBuilder routerBuilder = new WampRouterBuilder();
		routerBuilder.addRealm(realm);
		router = routerBuilder.build();

		URI serverUri = URI.create(wsUrl);
		server = new SimpleWampWebsocketListener(router, serverUri, null);
		server.start();
		logger.info("Router service started for realm '{}' at {}", realm, wsUrl);
	}

	public void stopRouter() {
		if (server != null) {
			router.close();
			server.stop();
			logger.info("Router service stopped.");
		} else {
			logger.warn("Router service not available, can't stop it.");
		}
	}

	/**
	 * Start a wamp client and provide its configuration.
	 * @param onConnected
	 */
	public void startClient(Consumer<WampClient> onConnected) throws Exception {

		if (wampClient != null) {
			logger.error("Cannot re-open existing wamp client, close the existing first.");
		}
		wampClient = buildWampClient();

		wampClient.statusChanged().subscribe((WampClient.State newState) -> {
					if (newState instanceof WampClient.ConnectedState) {

						// Client got connected to the router
						// and the session was established
						logger.info("Got connected to the router");

						// Register subscriptions and rpc functions
						onConnected.accept(wampClient);

					} else if (newState instanceof WampClient.DisconnectedState) {
						// Client got disconnected from the remote router
						// or the last possible connect attempt failed
						logger.info("Got disconnected from the wamp router");

					} else if (newState instanceof WampClient.ConnectingState) {
						// Client starts connecting to the remote router
						logger.info("Start connecting to the wamp router");

					}},

				// Output any errors or the Obsevable will swallow it
				Throwable::printStackTrace
		);

		logger.info("Opening wamp client connection to router..");
		wampClient.open();

	}

	/**
	 * Start a generic wamp client
	 */
	public void startClient() throws Exception {
		startClient(c -> {});
	}

	/**
	 * Start in webserver-configuration: Opens the wamp client connection pre-configured with event handling and
	 * data broadcasting from server to clients.
	 */
	public void startAsServer(BlitzController controller, Consumer<WampClient> onConnected) throws Exception {

		startClient((client) -> {

			// Register "ch.insign.server.call" which receives client events on the server
			client.registerProcedure(Blitz.URI_SERVER_CALL).subscribe((request) -> {

						String event = request.arguments().get(0).asText();
						// FIXME: request.details() never contains the caller id when called with '{disclose_me: true}') - Bug in Jawampa?
						// So we need to manually pass and add it here for now.
						request.details().set("caller", request.arguments().get(2));
						logger.info("Received event: " + event + ", data: " + request.arguments().toString() + ", details: " + request.details());

						// Call the controller's dispatch to route the event to its target
						controller.dispatchEvent(event, request, this);

						// Send a simple receive acknowledgement (note: transport-level only, higher-level errors should not be sent here)
						request.reply(true);
					},
					Throwable::printStackTrace);

			// Execute the provided configuration
			onConnected.accept(client);

		});
	}

	/**
	 * Start in webserver-configuration: Opens the wamp client connection pre-configured with event handlind and
	 * data broadcasting from server to clients.
	 */
	public void startAsServer(BlitzController controller) throws Exception {
		startAsServer(controller, c -> {});
	}


	public void stopClient() {
		wampClient.close()
				.subscribe((s) -> this.wampClient = null);
	}

	/**
	 * Start the connection to the router using our local router
	 * @return WampClient
	 */
	private WampClient buildWampClient() throws  Exception {
		return buildWampClient(wsUrl, realm);
	}

	/**
	 * Start the connection to the specified router.
	 *
	 * @param wsUrl Location of the web socket service
	 * @param realm Realm to be used
	 * @return WampClient
	 */
	private WampClient buildWampClient(String wsUrl, String realm) throws Exception {

		// Create a builder and configure the client
		WampClientBuilder builder = new WampClientBuilder();
		builder.withConnectorProvider(connectorProvider)
				.withUri(wsUrl)
				.withRealm(realm)
				.withInfiniteReconnects()
				.withReconnectInterval(3, TimeUnit.SECONDS);

		// Create a client through the builder. This will not immediatly start
		// a connection attempt
		return builder.build();
	}

	/**
	 * Publish a (global) data update.
	 *
	 * @param name the variable/data name
	 * @param data the data
	 */
	public void publish(String name, Object... data) {
		if (this.client() == null) {
			throw new IllegalStateException("Client is not connected.");
		}

		rx.Observable<Long> res = this.client().publish(URI_SERVER_DATA, name, data);
		logger.debug("Published '{}' -> '{}'", name, data);
		res.subscribe(
				id -> logger.trace("Published as {}", id),
				Throwable::printStackTrace);
	}

	/**
	 * Call a client to send a data update.
	 *
	 * @param name the variable/data name
	 * @param data the data
	 */
	public void call(String sid, String name, Object... data) {
		if (this.client() == null) {
			throw new IllegalStateException("Client is not connected.");
		}

		String uri = URI_CLIENT_CALL + sid;
		rx.Observable<Reply> res = this.client().call(uri, name, data);
		logger.info("Called client {}: {} -> {}", sid, name, data);
		res.subscribe(
				id -> logger.trace("Called {}", id),
				Throwable::printStackTrace);
	}

	/**
	 * Call the server (if connected as client) to send a data update.
	 *
	 * @param name the variable/data name
	 * @param data the data
	 */
	public void callServer(String name, Object... data) {
		if (this.client() == null) {
			throw new IllegalStateException("Client is not connected.");
		}

		String uri = URI_SERVER_CALL;
		rx.Observable<Reply> res = this.client().call(uri, name, data /* FIXME: needs my sid here */);
		logger.debug("Called server: {} -> {}", name, data);
		res.subscribe(
				id -> logger.trace("Called {}", id),
				Throwable::printStackTrace);
	}
}
