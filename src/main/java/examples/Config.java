package examples;

import com.typesafe.config.ConfigFactory;

import java.io.File;

/**
 * Config
 * Reads in a ./local.conf file and uses the supplied application.conf as fallback/default.
 */
public class Config {

	private final static com.typesafe.config.Config conf =
		ConfigFactory.parseFile(new File("./local.conf"))
			.withFallback(ConfigFactory.load());


	public final static String TEST_WS_URL = conf.getString("test.WS_URL");
	public static final String TEST_REALM = conf.getString("test.REALM");

	public final static String EXAMPLE_WS_URL = conf.getString("example.WS_URL");
	public static final String EXAMPLE_REALM = conf.getString("example.REALM");

}
