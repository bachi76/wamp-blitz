package examples.todo;

import ch.insign.blitz.Blitz;
import ch.insign.blitz.BlitzController;
import examples.Config;
import org.slf4j.LoggerFactory;

/**
 * Backend for the interactive to-do example
 */
public class BlitzTodoServer {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzTodoServer.class);

	public static void main(String[] args) {

		Blitz blitz = new Blitz(Config.EXAMPLE_WS_URL, Config.EXAMPLE_REALM);
		BlitzController controller = new BlitzTodoController();

		try {

			blitz.startRouter();
			blitz.startAsServer(controller, (wampClient -> {
				// Add some rpc registrations or other stuff on connect
			}));

		} catch (Exception e) {
			e.printStackTrace();
		}


		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


}
