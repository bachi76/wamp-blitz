package examples.todo;

import ch.insign.blitz.Blitz;
import ch.insign.blitz.BlitzController;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.LoggerFactory;
import ws.wamp.jawampa.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The to-do controller which receives client events
 */
public class BlitzTodoController implements BlitzController {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzTodoController.class);

	/*
	 server: {
                added: 0,
                completed: 0,
                todos: [],
                users: []
            }
	 */

	private int added;
	private int completed;
	private List<Todo> todos = new ArrayList<>();
	private Map<String, String> users = new HashMap<>();

	public void login(Request request, Blitz blitz) {
		JsonNode data = request.arguments().get(1);
		users.put(getSid(request), data.asText());

		blitz.publish("msg", "User " + users.get(getSid(request)) + " has joined.");
		blitz.publish("users", users.values());
	}

	public void save(Request request, Blitz blitz) {

		Todo t = new Todo();
		t.title = getData(request).asText();
		t.author = users.get(getSid(request));
		t.id = added;

		todos.add(t);
		added++;

		blitz.publish("todos", todos);
		blitz.publish("added", added);

		blitz.call(getSid(request), "msg", "Thanks for your ideas to a better world!");
	}

	public void done(Request request, Blitz blitz) {
		int tid = getData(request).asInt();

		if (todos.removeIf(e -> e.id == tid)) {
			completed ++;
			blitz.publish("todos", todos);
			blitz.publish("completed", completed);

			String msg = todos.size() > 0 ? "Well done completing that one, just " + todos.size() + " tasks left." : "Wow - all done!";
			blitz.call(getSid(request), "msg", msg);
		}
	}

	/**
	 * Send initial data upon new connection
	 */
	public void connected(Request request, Blitz blitz) {

		users.put(getSid(request), "connecting...");

		blitz.publish("todos", todos);
		blitz.publish("users", users.values());
		blitz.publish("completed", completed);
		blitz.publish("added", added);

		blitz.call(getSid(request), "msg", "Welcome " + getSid(request));
	}

	/**
	 * Send initial data upon new connection
	 */
	public void disconnected(Request request, Blitz blitz) {

		blitz.publish("msg", "User " + users.get(getSid(request)) + " just left.");

		users.remove(getSid(request));
		blitz.publish("users", users.values());
	}
}
