package examples.todo;

/**
 * POJO to transmit.
 *
 * Important note: You need to use getters and setters! If you do not,
 * it will not get serialized and transmitted (Jawampa uses Jackson, which by default
 * uses getters).
 */
public class Todo {
	int id;
	String title;
	String author;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
}
