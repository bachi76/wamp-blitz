package examples.testjava2java;

import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This bean gets sent over the websockets between the two nodes that run in their own jvms
 */
public class TestWiredBean implements Serializable {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(TestWiredBean.class);

    private int a = 0;
    private String String = "initial";
    private List<Integer> list = new ArrayList<>();

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public java.lang.String getString() {
        return String;
    }

    public void setString(java.lang.String string) {
        String = string;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public void update() {
        setA(getA() + 1);
        getList().add((int) (Math.random() * 100));
        setString(getString().equals("ping") ? "pong" : "ping");
    }

    @Override
    public java.lang.String toString() {
        return "TestWiredBean{" +
                "a=" + a +
                ", String='" + String + '\'' +
                ", list=" + list +
                '}';
    }
}
