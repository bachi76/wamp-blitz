package examples.testjava2java;

import ch.insign.blitz.Blitz;
import com.fasterxml.jackson.databind.ObjectMapper;
import examples.Config;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * A simple WAMP / jawampa test of sending back and forth POJOs via RPC and PubSub
 * Just start two instances of this class.
 */
public class BlitzTestTwoNodesExample {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzTestTwoNodesExample.class);

	private ObjectMapper mapper = new ObjectMapper();

	public static void main(String[] args) {
		BlitzTestTwoNodesExample server = new BlitzTestTwoNodesExample();
		server.run();
	}


	public void run() {
		Blitz blitz = new Blitz(Config.TEST_WS_URL, Config.TEST_REALM);

		boolean weAreSecond;

		try {
			blitz.startRouter();
			weAreSecond = false;

		} catch (Exception error) {
			logger.info("Router not started - probably already there.. we are the second node!", error);
			weAreSecond = true;
		}

		try {

			final int ourNum = weAreSecond? 2:1;

			blitz.startClient(wampClient -> {

                // Native pub sub
                blitz.client().makeSubscription("beanStation", TestWiredBean.class).subscribe((wiredBean) -> {
                    logger.info("Received via subscription: {}", wiredBean);

					// Answer via rpc ...
					wiredBean.update();
					blitz.client().call("beanCall" + (ourNum == 1 ? 2:1), wiredBean);

                });

				// Native rpc - register a beanCall procedure
				// Jawampa doesnt offer direct object mapping here it seems, so we use the jackson mapper manually
				blitz.client().registerProcedure("beanCall" + ourNum).subscribe((request) -> {
					try {
						TestWiredBean wiredBean = mapper.readValue(request.arguments().get(0).toString() , TestWiredBean.class);
						request.reply();

						logger.info("Received via rpc call: {}", wiredBean);

						// Answer via publish ...
						wiredBean.update();
						blitz.client().publish("beanStation", wiredBean);

					} catch (IOException e) {
						e.printStackTrace();
					}
				});

				logger.info("Connected!");

				// Someone has to start..
				if (ourNum == 2) {
					TestWiredBean firstBean = new TestWiredBean();
					firstBean.setString("Welcome!");

					blitz.client().call("beanCall1", firstBean);
				}
            });


		} catch (Exception e) {
			e.printStackTrace();
		}

		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}



