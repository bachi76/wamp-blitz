package examples.development;

import ch.insign.blitz.Blitz;
import ch.insign.blitz.BlitzController;
import org.slf4j.LoggerFactory;
import ws.wamp.jawampa.Request;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class MyBlitzController implements BlitzController {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzController.class);
	private int counter;
	private List<String> list = new ArrayList<>();

	public void save(Request request, Blitz blitz) {
		logger.info("got a click from " + request);
		blitz.publish("counter", counter++);

		list.add("Clicked at " + LocalDateTime.now());
		blitz.publish("list", list);

	}

	public void updateMessage(Request request, Blitz blitz) {
		String msg = request.arguments().get(1).asText();
		logger.info("updateMessage: " + msg);
		blitz.publish("counter", counter++);
		blitz.publish("text", msg);

		blitz.publish("test", "a", 3, list);
	}

	/**
	 * Send initial data upon new connection
	 */
	@Override
	public void connected(Request request, Blitz blitz) {
		blitz.publish("counter", ++counter);
		blitz.publish("list", list);
	}
}