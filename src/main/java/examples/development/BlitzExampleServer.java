package examples.development;

import ch.insign.blitz.Blitz;
import ch.insign.blitz.BlitzController;
import examples.Config;
import org.slf4j.LoggerFactory;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;

/**
 * The example main class
 */
public class BlitzExampleServer {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzExampleServer.class);

	static IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();

	public static void main(String[] args) {

		Blitz blitz = new Blitz(Config.EXAMPLE_WS_URL, Config.EXAMPLE_REALM);
		BlitzController controller = new MyBlitzController();

		try {

			blitz.startRouter();
			blitz.startAsServer(controller, (wampClient -> {
				// Add some rpc registrations or other stuff on connect
			}));


		} catch (Exception e) {
			e.printStackTrace();
		}


		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}