package examples.test;

import ch.insign.blitz.Blitz;
import ch.insign.blitz.BlitzController;
import com.fasterxml.jackson.databind.JsonNode;
import examples.Config;
import org.slf4j.LoggerFactory;
import ws.wamp.jawampa.Request;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;


/**
 * Run this server when running the frontend / js tests
 */
public class BlitzTestServer {

	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BlitzTestServer.class);

	static IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();

	class TestBlitzController implements BlitzController {

		public void add(Request request, Blitz blitz) {
			logger.info("add() called");

			int result = request.arguments().get(1).get(0).asInt() + request.arguments().get(1).get(1).asInt();
			blitz.client().publish("result", result);
		}

		public void ping(Request request, Blitz blitz) {
			logger.info("ping() called");
			blitz.call(getSid(request), "ping.response", "pong");
		}

		/**
		 * Send initial data upon new connection
		 */
		@Override
		public void connected(Request request, Blitz blitz) {

		}

	}

	public static void main(String[] args) {
		BlitzTestServer server = new BlitzTestServer();
		server.run();
	}


	public void run() {
		Blitz blitz = new Blitz(Config.TEST_WS_URL, Config.TEST_REALM);
		BlitzController controller = new TestBlitzController();

		try {

			blitz.startRouter();
			blitz.startAsServer(controller, wampClient -> {

				// Native pub sub - subscribe to receive events on channel "ping", then publish the result on channel "pong"
				blitz.client().makeSubscription("ping").subscribe((event) -> {

					int result = event.arguments().get(0).asInt() * event.arguments().get(1).asInt();
					logger.info("Received ping, sending pong with result " + result);
					blitz.client().publish("pong", result);
				});

				// Native rpc - register a helloServer procedure
				blitz.client().registerProcedure("helloServer").subscribe((request) -> {
					JsonNode data = request.arguments();
					logger.info("Called helloSever with this data: " + data.toString());
					String response = "Hello " + data.get(0).asText();
					request.reply(response);
				});

			});




		} catch (Exception e) {
			e.printStackTrace();
		}


//		Data data = new Data();
//		data.list.add("Some");
//		data.list.add("Entries");
//		data.list.add("Already");
//		data.list.add("Here");



		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}



