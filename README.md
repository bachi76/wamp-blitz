# Data Blitz #
#### *Trigger browser events on the server. Push server-initiated events to one or all connected clients. Websockets without all the boilerplate.* ####

Blitz combines the formidable [Autobahn.ws](http://autobahn.ws/) libraries and [Vue.js](https://vuejs.org/guide/) to interact through websockets between web server and web clients as if they were running on one machine.

## Basic example ##
Let's consider an interactive task list. Clicking the 'Add' button should trigger our server-side *save()* action and send the todo entry along. The server then updates the todo list and updates *all* connected clients instantly.

Note: You can see the [full example in action](http://blitz.bachi.ch/examples/todo/todo-interactive.html) here. Hint: Open it in multiple browser windows.

The full code can be found in the sever and client example folders.

### Server-side ###
For now, a Java backend is supported through the [Javampa](https://github.com/Matthias247/jawampa) implementation. PHP and Python backends should follow.

1) Create a Blitz controller. Client-side events are routed to the controller's actions. Example for a todo save method:

```java
	public class BlitzTodoController implements BlitzController {
	
		private List<Todo> todos = new ArrayList<>();
	
		public void save(Request request, Blitz blitz) {
	
			// Update server state
			Todo t = new Todo(getData(request).asText());
			todos.add(t);
	
			// Update the todo list of all connected clients
			blitz.publish("todos", todos);
			
			// Send a message to the originating client only
			blitz.call(getSid(request), "msg", "Thanks for your ideas!");
		}
	}
```

2) Create a Blitz instance and pass in the controller:
```java
	Blitz blitz = new Blitz("ws://localhost:8083/ws1", "exampleRealm");
	BlitzController controller = new BlitzTodoController();

	blitz.startRouter();
	blitz.startAsServer(controller, (wampClient -> {
		// Add additional rpc registrations on connect
	}));
		
```

### Client-side ###

1) Create a Vue.js model ([more](https://vuejs.org/guide/)). Place server-side data in a 'server' object inside data:
```javascript
    // Vue model configuration. 
    var vm = new Vue({
        data: {
            
            // This data is updated server-side
            server: {
                todos: [],
                msg: ''
            }
        },
        methods: {
            
            // Map call() to Blitz
            call: function(event, data) {
                blitz.call(event, data);
            }
        }
    });
    
    // And now let's blitz..
    var blitz = new VueBlitz('ws://' +  window.location.hostname +':8083/ws1', 'exampleRealm', vm);
    blitz.connect();
        
```

2) To *send events and data to the server*, simply **call** the server method (*save()* here) and send the data along. The call is routed to the server-side controller's matching method. Using Vue, this becomes as simple as this:

```
     <input v-model="input">
     <button type="button" v-on:click="call('save', input)">OK</button>
```
 
3) If the server sends data updates, the vue model is being updated and so are any instances of that data. Example:

```
     <div class="alert alert-success">
         {{server.msg}}
     </div>
```
 
That's all - clicks on the save button trigger the server-side controller action, and server-initiated data updates to one or all clients instantly update the clients' view models.

You can of course directly add more pub/sub or rpc features to your project using the normal Autobahn.ws features.

## Communication flow ##

The following diagram partially shows the communication flow between clients and the server. '#server.data' is the pub/sub channel used by Blitz where the server broadcasts global data updates to and Blitz clients subscribe to.

![Communication-Flow.png](https://bitbucket.org/repo/njgEGM/images/115392401-Communication-Flow.png)

## Native Autobahn.ws usage ##
Blitz is just a thin layer of over the underlying Autobahn.js and Vue.js libraries on the client and the Jawampa java implementation on the server. Blitz adds standardized data exchange and call routing. To add your own remote procedures or pub/sub channels, you can access the underlying Autobahn sessions directly.

#### Server example: ####
```java
blitz.startAsServer(controller, wampClient -> {

	// Native pub sub - subscribe to receive events on channel "ping", then publish the result on channel "pong"
	blitz.client().makeSubscription("ping").subscribe((event) -> {

		int result = event.arguments().get(0).asInt() * event.arguments().get(1).asInt();
		logger.info("Received ping, sending pong with result " + result);
		blitz.client().publish("pong", result);
	});

	// Native rpc - register a helloServer procedure
	blitz.client().registerProcedure("helloServer").subscribe((request) -> {
		JsonNode data = request.arguments();
		logger.info("Called helloSever with this data: " + data.toString());
		String response = "Hello " + data.get(0).asText();
		request.reply(response);
	});

});
```
For further information on the jawampa Autobahn Java implementation, consult https://github.com/Matthias247/jawampa

#### Client example: ####

```javascript
var blitz = new Blitz(wsUrl, realm);

    blitz.onConnectionOpen = function() {

        blitz.session.subscribe("pong", function(args) {
            assert.equal(args[0], 5*2, "Server sent result of arg0 * arg1")
            blitz.disconnect();
        });
        
        blitz.session.call("helloServer", ["Chasperli"]).then(
            function (res) {
                assert.equal(res, "Hello Chasperli", "Server responsed with greeting")           
            }
        );

        blitz.session.publish("ping", [5, 2]);
    };

    blitz.connect();
```
For further information, see the [Autobahn.js documentation](http://autobahn.ws/js/).

## Contributions welcome ##
Missing a feature or found a bug? Feedback and pull requests are most welcome. I'm especially looking for a *Python server implementation* using the Autobahn python library.


## Legal note ##

Copyright 2016 Martin Bachmann, [insign gmbh](http://www.insign.ch)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this library except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.