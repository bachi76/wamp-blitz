/**
 * Data Blitz V0.9
 *
 * Using Autobahn|JS to sync data between server(s) and client(s) in an
 * automated fashion.
 *
 * Set the 'onDataChanged' handler to wire your client model to the server updates.
 * An example for interactiing with the Vue.js library is given in the VueBlitz class.
 *
 * @author Martin Bachmann <m.bachmann@insign.ch>
 *
 */

/// <reference path="autobahn.d.ts" />

class Blitz {

    private static URI_PREFIX = "ch.insign.blitz.";
    private static URI_SERVER_CALL = Blitz.URI_PREFIX + "server.call" ;
    private static URI_SERVER_DATA = Blitz.URI_PREFIX + "server.data" ;
    private static URI_CLIENT_CALL = Blitz.URI_PREFIX + "client.data.to." ; // + sid
    private static EVENT_CONNECTED = "connected";
    private static EVENT_DISCONNECT = "disconnected";

    public debugMode: boolean = true;

    public onDataChanged: any = function(key, value) {console.log("onDataChanged: ", key, value)};
    public onEvent: any = function(event) {console.log("onEvent: ", event)};
    public onConnectionOpen: any = function() {console.log("onConnectionOpen")};
    public onConnectionClose: any = function(reason: String, detail: String) {console.log("onConnectionClose: ", detail)};
    public session: autobahn.Session;

    private connected: boolean = false;
    private wsUrl;
    private realm;
    private connection: autobahn.Connection;

    constructor(wsUrl: String, realm: String) {
        this.wsUrl = wsUrl;
        this.realm = realm;
    }


    public connect() {
        this.debug("Trying to connect to " + this.wsUrl);

        this.connection  = new autobahn.Connection({
            url: this.wsUrl,
            realm: this.realm,
            retry_if_unreachable: true
        });

        this.connection.onopen = (session) => {
            this.session = session;
            this.session.caller_disclose_me = true;
            this.session.publisher_disclose_me = true;
            this.connected = true;

            this.debug("Connection successfully opened");

            // Subscribe to global server data (server -> all clients)
            session.subscribe(Blitz.URI_SERVER_DATA, (payload) => {
                var key = payload[0];
                var data = payload[1];
                this.debug('Server published new data: ' + key + " -> " + data);
                this.onDataChanged(key, data);
            });

            // Register a server data change call (server -> a single client)
            var rpcUri = Blitz.URI_CLIENT_CALL + session.id;
            this.debug('Registering ' + rpcUri);
            session.register(rpcUri, (payload) => {
                var key = payload[0];
                var data = payload[1];
                this.debug('Server sent us new data: ' + key + " -> " + data);
                this.onDataChanged(key, data);
            });

            // Disconnect when closing the browser window
            window.addEventListener("beforeunload", (e) => this.disconnect());


            // The 'connected' call is sent immediately after establishing the blitz connection
            this.call(Blitz.EVENT_CONNECTED, null);

            this.onConnectionOpen();
        };

        this.connection.onclose = (reason: String, details: String) => {
            this.connected = false;
            this.onConnectionClose(reason, details);
            return true;
        };

        this.connection.open();
        this.debug("connection.open() called... ");
    }

    public disconnect() {
        this.call(Blitz.EVENT_DISCONNECT, null);
        this.connection.close();
    }

    public isConnected(): boolean {
        return this.connected;
    }

    /**
     * Send an call to the server
     * (sent using a rpc call as it's meant for one recipient only)
     * @param event name
     * @param data
     */
    public call(event: String, data: any) {
        var sid = this.session.id;
        // FIXME: in Jawampa, request.details() never contains the caller id when called with '{disclose_me: true}') - Bug in Jawampa?
        // So we need to manually pass and add it here for now.
        this.session.call(Blitz.URI_SERVER_CALL, [event, data, sid], {}, {disclose_me: true})
            .then((res) => {
                    if (res === true) {
                        this.debug("Event '" + event + "' sent to server");
                    } else {
                        this.error("Event '" + event + "' likely not sent to server (got no ack signal back)");
                    }
                }
            );
    }

    private debug(msg: String) {
        if (this.debugMode) console.log(msg);
    }

    private error(msg: string) {
        console.error(msg);

    }
}

/**
 * Use this class if you intend to use Blitz together with Vue.js
 * It will automatically update the Vue model's data (node "server") when the server
 * publishes new data.
 */
class VueBlitz extends Blitz {

    /**
     * The vue model needs to define the server data (and optionally initial values), furhter server-published
     * data updates will update the vue model accordingly.
     *
     * @param wsUrl
     * @param realm
     * @param vue
     */
    constructor(wsUrl: String, realm: String, vue) {
        super(wsUrl, realm);

        this.onDataChanged = function(key, value) {
            // If a sigle object is transmitted, "unpack" it - otherwise leave it in the array
            if (value.length == 1) value = value[0];
            console.log("Updating vue model data: " + key + " -> " + value);
            vue.server[key] = value;
        };
    }
}
