/**
 * Data Blitz V0.9
 *
 * Using Autobahn|JS to sync data between server(s) and client(s) in an
 * automated fashion.
 *
 * Set the 'onDataChanged' handler to wire your client model to the server updates.
 * An example for interactiing with the Vue.js library is given in the VueBlitz class.
 *
 * @author Martin Bachmann <m.bachmann@insign.ch>
 *
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="autobahn.d.ts" />
var Blitz = (function () {
    function Blitz(wsUrl, realm) {
        this.debugMode = true;
        this.onDataChanged = function (key, value) { console.log("onDataChanged: ", key, value); };
        this.onEvent = function (event) { console.log("onEvent: ", event); };
        this.onConnectionOpen = function () { console.log("onConnectionOpen"); };
        this.onConnectionClose = function (reason, detail) { console.log("onConnectionClose: ", detail); };
        this.connected = false;
        this.wsUrl = wsUrl;
        this.realm = realm;
    }
    Blitz.prototype.connect = function () {
        var _this = this;
        this.debug("Trying to connect to " + this.wsUrl);
        this.connection = new autobahn.Connection({
            url: this.wsUrl,
            realm: this.realm,
            retry_if_unreachable: true
        });
        this.connection.onopen = function (session) {
            _this.session = session;
            _this.session.caller_disclose_me = true;
            _this.session.publisher_disclose_me = true;
            _this.connected = true;
            _this.debug("Connection successfully opened");
            // Subscribe to global server data (server -> all clients)
            session.subscribe(Blitz.URI_SERVER_DATA, function (payload) {
                var key = payload[0];
                var data = payload[1];
                _this.debug('Server published new data: ' + key + " -> " + data);
                _this.onDataChanged(key, data);
            });
            // Register a server data change call (server -> a single client)
            var rpcUri = Blitz.URI_CLIENT_CALL + session.id;
            _this.debug('Registering ' + rpcUri);
            session.register(rpcUri, function (payload) {
                var key = payload[0];
                var data = payload[1];
                _this.debug('Server sent us new data: ' + key + " -> " + data);
                _this.onDataChanged(key, data);
            });
            // Disconnect when closing the browser window
            window.addEventListener("beforeunload", function (e) { return _this.disconnect(); });
            // The 'connected' call is sent immediately after establishing the blitz connection
            _this.call(Blitz.EVENT_CONNECTED, null);
            _this.onConnectionOpen();
        };
        this.connection.onclose = function (reason, details) {
            _this.connected = false;
            _this.onConnectionClose(reason, details);
            return true;
        };
        this.connection.open();
        this.debug("connection.open() called... ");
    };
    Blitz.prototype.disconnect = function () {
        this.call(Blitz.EVENT_DISCONNECT, null);
        this.connection.close();
    };
    Blitz.prototype.isConnected = function () {
        return this.connected;
    };
    /**
     * Send an call to the server
     * (sent using a rpc call as it's meant for one recipient only)
     * @param event name
     * @param data
     */
    Blitz.prototype.call = function (event, data) {
        var _this = this;
        var sid = this.session.id;
        // FIXME: in Jawampa, request.details() never contains the caller id when called with '{disclose_me: true}') - Bug in Jawampa?
        // So we need to manually pass and add it here for now.
        this.session.call(Blitz.URI_SERVER_CALL, [event, data, sid], {}, { disclose_me: true })
            .then(function (res) {
            if (res === true) {
                _this.debug("Event '" + event + "' sent to server");
            }
            else {
                _this.error("Event '" + event + "' likely not sent to server (got no ack signal back)");
            }
        });
    };
    Blitz.prototype.debug = function (msg) {
        if (this.debugMode)
            console.log(msg);
    };
    Blitz.prototype.error = function (msg) {
        console.error(msg);
    };
    Blitz.URI_PREFIX = "ch.insign.blitz.";
    Blitz.URI_SERVER_CALL = Blitz.URI_PREFIX + "server.call";
    Blitz.URI_SERVER_DATA = Blitz.URI_PREFIX + "server.data";
    Blitz.URI_CLIENT_CALL = Blitz.URI_PREFIX + "client.data.to."; // + sid
    Blitz.EVENT_CONNECTED = "connected";
    Blitz.EVENT_DISCONNECT = "disconnected";
    return Blitz;
}());
/**
 * Use this class if you intend to use Blitz together with Vue.js
 * It will automatically update the Vue model's data (node "server") when the server
 * publishes new data.
 */
var VueBlitz = (function (_super) {
    __extends(VueBlitz, _super);
    /**
     * The vue model needs to define the server data (and optionally initial values), furhter server-published
     * data updates will update the vue model accordingly.
     *
     * @param wsUrl
     * @param realm
     * @param vue
     */
    function VueBlitz(wsUrl, realm, vue) {
        _super.call(this, wsUrl, realm);
        this.onDataChanged = function (key, value) {
            // If a sigle object is transmitted, "unpack" it - otherwise leave it in the array
            if (value.length == 1)
                value = value[0];
            console.log("Updating vue model data: " + key + " -> " + value);
            vue.server[key] = value;
        };
    }
    return VueBlitz;
}(Blitz));
//# sourceMappingURL=data-blitz.js.map