/**
 * QUnit Tests
 * Start "BlitzTestServer.java" first.
 */


var wsUrl = "ws://0.0.0.0:8087";
var realm = "test";

// dataBlitz.onDataChanged = function(data) {
//     console.log("Updating vue model data..");
//     vm.server = data[0]; // somehow arrray[0] contains the actual data object
// };


/* Test connection */
QUnit.test( "Connection to server", function( assert ) {

    var blitz = new Blitz(wsUrl, realm);

    var done1 = assert.async();
    var done2 = assert.async();

    blitz.onConnectionOpen = function() {
        assert.equal(blitz.isConnected(), true, "Blitz has connected");
        done1();

        blitz.disconnect();
    };
    blitz.onConnectionClose = function(reason, detail) {
        assert.equal(blitz.isConnected(), false, "Blitz has disconnected");
        done2();
    };
    blitz.connect();
});




// QUnit.test("XXX", function(assert) {
//     var dataBlitz = new Blitz(wsUrl, realm);
//     var done = assert.async();
//
//     dataBlitz.onConnectionOpen = function() {
//
//
//         done();
//         dataBlitz.disconnect();
//     };
//     dataBlitz.connect();
// });



QUnit.test("Call server method the native autobahn way", function(assert) {
    var blitz = new Blitz(wsUrl, realm);
    var done = assert.async();

    blitz.onConnectionOpen = function() {

        blitz.session.call("helloServer", ["Chasperli"]).then(
            function (res) {
                assert.equal(res, "Hello Chasperli", "Server responsed with greeting")
                done();
            }
        );
    };

    blitz.connect();
});


QUnit.test("Send and receive native autobahn events", function(assert) {
    var blitz = new Blitz(wsUrl, realm);
    var done = assert.async();

    blitz.onConnectionOpen = function() {

        blitz.session.subscribe("pong", function(args) {
            assert.equal(args[0], 5*2, "Server sent result of argo * arg1")

            done();
            blitz.disconnect();
        });

        blitz.session.publish("ping", [5, 2]);
    };

    blitz.connect();
});


QUnit.test("Send and receive client->server blitz events", function(assert) {
    var blitz = new Blitz(wsUrl, realm);
    var done = assert.async();

    blitz.onConnectionOpen = function() {

        blitz.session.subscribe("result", function(args) {
            assert.equal(args[0], 4+5, "Server sent result of argo + arg1")

            done();
            blitz.disconnect();
        });

        // This is what we test
        blitz.call("add", [4, 5]);
    };

    blitz.connect();
});

QUnit.test("Send and receive server->single client blitz call", function(assert) {
    var blitz = new Blitz(wsUrl, realm);
    var done = assert.async();

    blitz.onDataChanged = function(key, value) {
        console.log(key, value);
        assert.equal(key, 'ping.response', "Server sent a pong");
        assert.equal(value, 'pong', "Server sent a pong");
        done();
        blitz.disconnect();
    };

    blitz.onConnectionOpen = function() {
        blitz.call("ping");
    };

    blitz.connect();
});

